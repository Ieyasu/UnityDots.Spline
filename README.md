# UnityDots Splines

UnityDots Splines contains spline objects for the new Unity DOTS stack.

## Features

* Path/Bezier/CatmullRom splines
* Adaptive or uniform sampling of splines
* Debug visualization in the scene view

## Getting started

To include the package in your Unity project, add the following git packages using the Package Manager UI:

```
https://gitlab.com/Ieyasu/UnityDots.Mathematics.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Common.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Spline.git?path=/src"
```

The easiest way to create a new spline is to add a `SplineAuthoring` MonoBehaviour to a `GameObject`.

Alternatively, you can create splines from code. The following snippet shows how to create a new bezier spline that is drawn in the scene view:

```cs
void CreateBezierSpline(Entity entity, EntityManager entityManager)
{
    // Choose the spline type. Possibilities include:
    // * Path - A straight path going through the control points
    // * Bezier - A cubic bezier curve
    // * CatmullRom - A catmull rom curve
    entityManager.AddComponentData(entity, new Bezier());

    // After the spline is evaluated, the SplineSample buffer contains samples along the spline.
    // These are usually the points you want to use in your own systems.
    entityManager.AddBuffer<SplineSample>(entity);

    // Add a sampling mode that determines how samples are distributed on the spline:
    // * AdaptiveSampling - Places more samples where the spline has a steeper curvature
    // * UniformSampling - Places samples uniformly on the spline
    var sampling = new UniformSampling { Frequency = 0.5f };
    // var sampling = new AdaptiveSampline { MaxError = 0.1f };
    entityManager.AddComponentData(entity, sampling);

    // Add control points for the curve
    var controlPoints = entityManager.AddBuffer<ControlPoint>(entity);
    controlPoints.ResizeUninitialized(4);
    controlPoints[0] = new ControlPoint { Position = new float3(0, 0, 0), Scale = 1, Roll = 0 };
    controlPoints[1] = new ControlPoint { Position = new float3(10, 0, 0), Scale = 1, Roll = 0 };
    controlPoints[2] = new ControlPoint { Position = new float3(10, 10, 0), Scale = 1, Roll = 0 };
    controlPoints[3] = new ControlPoint { Position = new float3(0, 10, 0), Scale = 1, Roll = 0 };

    // Add debug drawing to visualize the spline on scene view
    entityManager.AddComponentData(entity, new SplineDebugPositionsTag());
    entityManager.AddComponentData(entity, new SplineDebugTangentsTag());
    entityManager.AddComponentData(entity, new SplineDebugNormalsTag());
}
```
