using System.Collections.Generic;
using UnityDots.Splines.Authoring;

namespace UnityDots.Splines.Editors
{
    public enum SplineNodeSelectionType
    {
        Node,
        StartHandle,
        EndHandle
    }

    public struct SplineNodeSelection
    {
        public Node Node;
        public SplineNodeSelectionType Type;
    }

    public static class SplineToolSelection
    {
        private static readonly Dictionary<SplineAuthoring, SplineNodeSelection> _selections = new Dictionary<SplineAuthoring, SplineNodeSelection>();

        public static bool HasSelection(SplineAuthoring target)
        {
            Validate(target);
            return _selections.ContainsKey(target);
        }

        public static bool IsSelected(SplineAuthoring target, Node node)
        {
            Validate(target);
            if (_selections.TryGetValue(target, out SplineNodeSelection selection))
            {
                return node == selection.Node;
            }
            return false;
        }

        public static SplineNodeSelection? GetSelection(SplineAuthoring target)
        {
            Validate(target);
            return _selections.TryGetValue(target, out SplineNodeSelection selection) ? (SplineNodeSelection?)selection : null;
        }

        public static void Select(SplineAuthoring target, Node node, SplineNodeSelectionType type = SplineNodeSelectionType.Node)
        {
            _selections[target] = new SplineNodeSelection
            {
                Node = node,
                Type = type
            };
        }

        public static void Deselect(SplineAuthoring target)
        {
            _selections.Remove(target);
        }

        private static void Validate(SplineAuthoring target)
        {
            if (_selections.TryGetValue(target, out SplineNodeSelection selection) && !target.Nodes.Contains(selection.Node))
            {
                _selections.Remove(target);
            }
        }
    }
}
