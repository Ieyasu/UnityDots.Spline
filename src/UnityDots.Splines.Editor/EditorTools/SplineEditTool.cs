using UnityEngine;
using UnityEditor.EditorTools;
using UnityDots.Splines.Authoring;
using UnityEditor;

namespace UnityDots.Splines.Editors
{
    [EditorTool("Spline Edit Tool", typeof(SplineAuthoring))]
    public sealed class SplineEditTool : SplineTool
    {
        protected override Texture2D LightIcon => Resources.Load<Texture2D>("SplineEditToolLight");
        protected override Texture2D DarkIcon => Resources.Load<Texture2D>("SplineEditToolDark");
        protected override string Name => "Spline Edit Tool";
        protected override string Tooltip => "Spline Tool to move, add and remove control points";
        protected override string UndoMessage => "Edit Control Point";

        private void OnDisable()
        {
            SplineToolSelection.Deselect(Target);
        }

        protected override void Draw()
        {
            foreach (var node in Target.Nodes)
            {
                DrawHandle(node);
                DrawSelectedHandle(node);
            }

            HandleEvents();
        }

        private void DrawSelectedHandle(Node node)
        {
            var selection = SplineToolSelection.GetSelection(Target);
            if (!selection.HasValue || selection.Value.Node != node)
            {
                return;
            }

            if (Target.Type == SplineType.Bezier && selection.Value.Type == SplineNodeSelectionType.StartHandle)
            {
                SplineToolUtility.SetStartHandle(node, SplineToolUtility.DrawHandleSelected(node.StartHandle));
            }
            if (Target.Type == SplineType.Bezier && selection.Value.Type == SplineNodeSelectionType.EndHandle)
            {
                SplineToolUtility.SetEndHandle(node, SplineToolUtility.DrawHandleSelected(node.EndHandle));
            }
            if (selection.Value.Type == SplineNodeSelectionType.Node)
            {
                node.Position = SplineToolUtility.DrawHandleSelected(node.Position);
            }
        }

        private void DrawHandle(Node node)
        {
            if (Target.Type == SplineType.Bezier)
            {
                SplineToolUtility.DrawTangents(node);
                DrawHandle(node, SplineNodeSelectionType.StartHandle, node.StartHandle, true);
                DrawHandle(node, SplineNodeSelectionType.EndHandle, node.EndHandle, true);
            }
            DrawHandle(node, SplineNodeSelectionType.Node, node.Position);
        }

        private void DrawHandle(Node node, SplineNodeSelectionType type, Vector3 position, bool isTangent = false)
        {
            var selection = SplineToolSelection.GetSelection(Target);
            if (selection.HasValue && selection.Value.Node == node && selection.Value.Type == type)
            {
                return;
            }

            if (SplineToolUtility.DrawHandle(position, isTangent))
            {
                SplineToolSelection.Select(Target, node, type);
            }
        }

        private void HandleEvents()
        {
            switch (Event.current.type)
            {
                case EventType.KeyDown:
                    HandleKeyDownEvent();
                    break;
            }
        }

        private void HandleKeyDownEvent()
        {
            switch (Event.current.keyCode)
            {
                case KeyCode.D:
                    DeleteSelectedNode();
                    break;
                case KeyCode.E:
                    ExtrudeSelectedNode();
                    break;
            }
        }

        private void DeleteSelectedNode()
        {
            if (Event.current.control || Event.current.alt || Event.current.command || Event.current.shift)
            {
                return;
            }
            Event.current.Use();

            if (SplineToolSelection.HasSelection(Target))
            {
                Undo.RegisterCompleteObjectUndo(Target, "Delete Node");
                Target.Nodes.Remove(SplineToolSelection.GetSelection(Target).Value.Node);
            }
        }

        private void ExtrudeSelectedNode()
        {
            if (Event.current.control || Event.current.alt || Event.current.command || Event.current.shift)
            {
                return;
            }
            Event.current.Use();

            if (!SplineToolSelection.HasSelection(Target) || Target.Nodes.Count < 2)
            {
                return;
            }

            Undo.RegisterCompleteObjectUndo(Target, "Add Node");
            var selection = SplineToolSelection.GetSelection(Target).Value.Node;
            if (selection == Target.Nodes[Target.Nodes.Count - 1])
            {
                var direction = 0.5f * (Target.Nodes[Target.Nodes.Count - 1].Position - Target.Nodes[Target.Nodes.Count - 2].Position);
                var node = new Node(Target.Nodes[Target.Nodes.Count - 1]);
                node.Position += direction;
                Target.Nodes.Add(node);
                SplineToolSelection.Select(Target, node);
            }
            else if (selection == Target.Nodes[0])
            {
                var direction = 0.5f * (Target.Nodes[0].Position - Target.Nodes[1].Position);
                var node = new Node(Target.Nodes[0]);
                node.Position += direction;
                Target.Nodes.Insert(0, node);
                SplineToolSelection.Select(Target, node);
            }
            else
            {
                var index = Target.Nodes.IndexOf(selection);
                var direction = 0.5f * (Target.Nodes[index + 1].Position - Target.Nodes[index].Position);
                var node = new Node(Target.Nodes[index]);
                node.Position += direction;
                Target.Nodes.Insert(index + 1, node);
                SplineToolSelection.Select(Target, node);
            }
        }
    }
}
