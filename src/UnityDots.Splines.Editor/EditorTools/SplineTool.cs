using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityDots.Splines.Authoring;

namespace UnityDots.Splines.Editors
{
    public abstract class SplineTool : EditorTool
    {
        // private static readonly string _iconPath = "Packages/com.unitydots.splines/UnityDots.Splines.Editor/Editor/Resources";

        private GUIContent _darkIconContent;
        private GUIContent _lightIconContent;
        public override GUIContent toolbarIcon => ToolManager.IsActiveTool(this) ? LightIconContent : DarkIconContent;

        protected SplineAuthoring Target => target as SplineAuthoring;
        protected virtual Color HandleColor => SplineToolUtility.HandleColor;

        protected abstract Texture2D DarkIcon { get; }
        protected abstract Texture2D LightIcon { get; }
        protected abstract string Name { get; }
        protected abstract string Tooltip { get; }
        protected abstract string UndoMessage { get; }
        protected abstract void Draw();

        private GUIContent DarkIconContent
        {
            get
            {
                if (_darkIconContent == null)
                {
                    _darkIconContent = GetToolbarIconContent(DarkIcon);
                }
                return _darkIconContent;
            }
        }

        private GUIContent LightIconContent
        {
            get
            {
                if (_lightIconContent == null)
                {
                    _lightIconContent = GetToolbarIconContent(LightIcon);
                }
                return _lightIconContent;
            }
        }

        private GUIContent GetToolbarIconContent(Texture2D icon)
        {
            return new GUIContent()
            {
                image = icon,
                text = Name,
                tooltip = Tooltip
            };
        }

        public override void OnToolGUI(EditorWindow window)
        {
            if (Target == null)
            {
                return;
            }

            EditorGUI.BeginChangeCheck();

            using (new Handles.DrawingScope(HandleColor, Target.transform.localToWorldMatrix))
            {
                Draw();
            }

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(Target, UndoMessage);
            }
        }
    }
}
