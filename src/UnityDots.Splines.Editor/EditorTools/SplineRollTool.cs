// using UnityDots.Splines.Authoring;
// using UnityEditor.EditorTools;
// using UnityEngine;

// namespace UnityDots.Splines.Editors
// {
//     [EditorTool("Spline Roll Tool", typeof(SplineAuthoring))]
//     public sealed class SplineRollTool : SplineTool
//     {
//         [SerializeField]
//         private Texture _toolIcon = null;

//         protected override string UndoMessage => "Roll Control Point";

//         protected override GUIContent GetToolbarIcon()
//         {
//             return new GUIContent()
//             {
//                 image = _toolIcon,
//                 text = "Spline Roll Tool",
//                 tooltip = "Spline Tool to adjust control point roll."
//             };
//         }

//         protected override void DrawHandles(Node node)
//         {
//             // var tangent =
//             // var rotation = Handles.Disc(Quaternion.identity, node.Position, Vector3.up, 0.1f, false, 0);
//         }
//     }
// }
