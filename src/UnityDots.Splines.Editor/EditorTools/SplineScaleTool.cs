// using UnityEngine;
// using UnityEditor.EditorTools;
// using UnityDots.Splines.Authoring;

// namespace UnityDots.Splines.Editors
// {
//     [EditorTool("Spline Scale Tool", typeof(SplineAuthoring))]
//     public sealed class SplineScaleTool : SplineTool
//     {
//         [SerializeField]
//         private Texture _toolIcon = null;

//         protected override string UndoMessage => "Scale Control Point";

//         protected override GUIContent GetToolbarIcon()
//         {
//             return new GUIContent()
//             {
//                 image = _toolIcon,
//                 text = "Spline Scale Tool",
//                 tooltip = "Spline Tool to change control point scale."
//             };
//         }

//         protected override void DrawHandles(Node node)
//         {
//             // var result = Handles.ScaleHandle(_scale, node.Position, GetRotation(node), 1);
//             // if (result != _scale)
//             // {
//             //     node.StartTangent += Vector3.Scale(node.StartTangent.normalized, result - _scale);
//             //     node.EndTangent = node.EndTangent.magnitude * -node.StartTangent.normalized;
//             //     _scale = result;
//             // }
//         }
//     }
// }
