using UnityEngine;
using UnityEditor;
using UnityDots.Splines.Authoring;
using UnityDots.Editor;

namespace UnityDots.Splines.Editors
{
    public static class SplineToolUtility
    {
        public static readonly float HandleScale = 0.08f;
        public static readonly float HandleOpacity = 0.9f;
        public static readonly float HandleTangentOpacity = 0.5f;
        public static readonly float HandleWidth = 3;

        public static readonly Color HandleColor = new Color(1.0f, 1.0f, 1.0f, HandleOpacity);
        public static readonly Color HandleTangentColor = new Color(1.0f, 0.1f, 0.1f, HandleTangentOpacity);

        public static readonly int CurveWidth = 3;
        public static readonly Color CurveColor = Color.white;
        public static readonly Color TangentColor = new Color(0.9f, 0.1f, 0.1f);

        public static bool DrawHandle(Vector3 position, bool isTangent = false)
        {
            var size = HandleScale * HandleUtility.GetHandleSize(position);
            if (Event.current.type == EventType.MouseDown && HandleUtility.DistanceToCircle(position, 0.001f) < 11)
            {
                Event.current.Use();
                return true;
            }

            var color = Handles.color;
            Handles.color = isTangent ? HandleTangentColor : HandleColor;
            Handles.FreeMoveHandle(position, Quaternion.identity, size, Vector3.zero, Handles.DotHandleCap);
            Handles.color = color;
            return false;
        }

        public static Vector3 DrawHandleSelected(Vector3 position)
        {
            return Handles.PositionHandle(position, Quaternion.identity);
        }

        public static void SetStartTangent(Node node, Vector3 tangent)
        {
            node.StartTangent = tangent;
            node.EndTangent = node.EndTangent.magnitude * -node.StartTangent.normalized;
        }

        public static void SetStartHandle(Node node, Vector3 handle)
        {
            SetStartTangent(node, handle - node.Position);
        }

        public static void SetEndTangent(Node node, Vector3 tangent)
        {
            node.EndTangent = tangent;
            node.StartTangent = node.StartTangent.magnitude * -node.EndTangent.normalized;
        }

        public static void SetEndHandle(Node node, Vector3 handle)
        {
            SetEndTangent(node, handle - node.Position);
        }

        public static void DrawTangents(Node node)
        {
            using (new Handles.DrawingScope(TangentColor))
            {
                HandlesUtil.DrawAALine(HandleWidth, node.Position, node.StartHandle);
                HandlesUtil.DrawAALine(HandleWidth, node.Position, node.EndHandle);
            }
        }
    }
}
