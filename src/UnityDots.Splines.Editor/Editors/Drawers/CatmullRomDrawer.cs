using Unity.Collections;

namespace UnityDots.Splines.Editors
{
    public sealed class CatmullRomDrawer : SplineDrawer<CatmullRom, CatmullRomSampleProvider>
    {
        protected override CatmullRom GetSpline()
        {
            return new CatmullRom { Alpha = Target.CatmullRomAlpha };
        }

        protected override int GetControlPointCount()
        {
            return Target.GetCatmullRomControlPointCount();
        }

        protected override void GetControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            Target.GetCatmullRomControlPoints(controlPoints);
        }
    }
}
