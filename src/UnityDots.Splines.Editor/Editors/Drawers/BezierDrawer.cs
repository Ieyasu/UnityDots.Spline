using Unity.Collections;

namespace UnityDots.Splines.Editors
{
    public sealed class BezierDrawer : SplineDrawer<Bezier, BezierSampleProvider>
    {
        public static readonly int HandleWidth = 3;

        protected override Bezier GetSpline()
        {
            return new Bezier();
        }

        protected override int GetControlPointCount()
        {
            return Target.GetBezierControlPointCount();
        }

        protected override void GetControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            Target.GetBezierControlPoints(controlPoints);
        }
    }
}
