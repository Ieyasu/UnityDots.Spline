using UnityEditor;
using UnityEngine;
using UnityDots.Collections;
using UnityDots.Splines.Authoring;
using Unity.Collections;
using System;
using Unity.Mathematics;

namespace UnityDots.Splines.Editors
{
    public abstract class SplineDrawer<TSpline, TSampleProvider> : ISplineDrawer, IDisposable
            where TSpline : struct, ISpline
            where TSampleProvider : struct, ISampleProvider<TSpline>
    {
        private static readonly ResizableArray<Vector3> _samplePositions = new ResizableArray<Vector3>();

        private SplineSampler<TSpline, TSampleProvider> _sampler;
        private NativeList<ControlPoint> _controlPoints;
        private NativeList<SplineSample> _samples;

        public SplineAuthoring Target { get; set; }
        public int SampleCount => _samples.Length;

        protected abstract TSpline GetSpline();
        protected abstract int GetControlPointCount();
        protected abstract void GetControlPoints(NativeArray<ControlPoint> controlPoints);


        public SplineDrawer()
        {
            _sampler = new SplineSampler<TSpline, TSampleProvider>(Allocator.Persistent);
            _controlPoints = new NativeList<ControlPoint>(Allocator.Persistent);
            _samples = new NativeList<SplineSample>(Allocator.Persistent);
        }

        public void Dispose()
        {
            _sampler.Dispose();
            _controlPoints.Dispose();
            _samples.Dispose();
        }

        public void Update()
        {
            _controlPoints.ResizeUninitialized(GetControlPointCount());
            GetControlPoints(_controlPoints.AsArray());
            _sampler.SetSpline(GetSpline(), _controlPoints);

            switch (Target.SamplingMethod)
            {
                case SamplingMethod.Adaptive:
                    _sampler.SampleAdaptive(Target.SamplingMaxError, _samples);
                    break;
                case SamplingMethod.Uniform:
                    _sampler.SampleUniform(Target.SamplingFrequency, _samples);
                    break;
            }

            _samplePositions.Clear();
            for (var i = 0; i < _samples.Length; ++i)
            {
                _samplePositions.Add(_samples[i].Position);
            }
        }

        public void DrawSpline()
        {
            using (new Handles.DrawingScope(Target.transform.localToWorldMatrix))
            {
                Handles.color = SplineToolUtility.CurveColor;
                Handles.DrawAAPolyLine(SplineToolUtility.CurveWidth, _samplePositions.Count, _samplePositions.Items);

                if (Target.DebugNormals)
                {
                    Handles.color = Color.magenta;
                    for (var i = 0; i < _samples.Length; ++i)
                    {
                        var sample = _samples[i];
                        var normal = math.mul(sample.RotationWithRoll, new float3(0, 1, 0));
                        Handles.DrawLine(sample.Position, sample.Position + Target.DebugVectorLength * normal);
                    }
                }

                if (Target.DebugTangents)
                {
                    Handles.color = Color.cyan;
                    for (var i = 0; i < _samples.Length; ++i)
                    {
                        var sample = _samples[i];
                        Handles.DrawLine(sample.Position, sample.Position + Target.DebugVectorLength * sample.Tangent);
                    }
                }
            }
        }
    }
}
