using UnityDots.Splines.Authoring;

namespace UnityDots.Splines.Editors
{
    public interface ISplineDrawer
    {
        SplineAuthoring Target
        {
            get;
            set;
        }

        int SampleCount
        {
            get;
        }

        void DrawSpline();
        void Update();
    }
}
