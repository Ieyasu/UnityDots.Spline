using Unity.Collections;

namespace UnityDots.Splines.Editors
{
    public sealed class PathDrawer : SplineDrawer<Path, PathSampleProvider>
    {
        protected override Path GetSpline()
        {
            return new Path();
        }

        protected override int GetControlPointCount()
        {
            return Target.GetPathControlPointCount();
        }

        protected override void GetControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            Target.GetPathControlPoints(controlPoints);
        }
    }
}
