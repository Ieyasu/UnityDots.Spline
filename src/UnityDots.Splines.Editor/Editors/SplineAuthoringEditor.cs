using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityDots.Splines.Authoring;
using UnityEditor.EditorTools;
using UnityEditor.UIElements;

namespace UnityDots.Splines.Editors
{
    [CustomEditor(typeof(SplineAuthoring))]
    public sealed class SplineAuthoringEditor : UnityEditor.Editor
    {
        private static Type _prevToolType;

        private ISplineDrawer _drawer;
        private SplineType _previousType;
        private SplineAuthoring Target => target as SplineAuthoring;

        private VisualElement _alphaSlider;
        private VisualElement _samplingFrequencySlider;
        private VisualElement _samplingMaxErrorSlider;
        private Label _infoText;

        public override VisualElement CreateInspectorGUI()
        {
            _alphaSlider = new SliderField("Alpha", 0.0f, 1.0f)
            {
                BindingPath = "CatmullRomAlpha"
            };

            _samplingMaxErrorSlider = new SliderField("Max Error", 0.001f, 1.0f, 4)
            {
                BindingPath = "SamplingMaxError"
            };

            _samplingFrequencySlider = new SliderField("Frequency", 0.01f, 1)
            {
                BindingPath = "SamplingFrequency"
            };

            var container = new VisualElement();
            container.LoadAndAddStyleSheet("spline_editor");

            var nodesProperty = serializedObject.FindProperty("Nodes");
            var typeField = new EnumField("Type", Target.Type) { bindingPath = "Type" };
            var samplingField = new EnumField("Method", Target.SamplingMethod) { bindingPath = "SamplingMethod" };
            var debugSlider = new SliderField("Debug Scale", 0.0f, 1.0f)
            {
                BindingPath = "DebugVectorLength"
            };
            var infoField = new InfoText("Samples", "-");
            var nodesField = new PropertyField(nodesProperty);
            _infoText = infoField.ElementAt(1) as Label;

            container.Add(typeField);
            container.Add(_alphaSlider);

            container.Add(new Title("Sampling"));
            container.Add(samplingField);
            container.Add(_samplingMaxErrorSlider);
            container.Add(_samplingFrequencySlider);
            container.Add(infoField);

            container.Add(new Title("Debug"));
            container.AddField(serializedObject, "DebugNormals");
            container.AddField(serializedObject, "DebugTangents");
            container.Add(debugSlider);

            container.Add(nodesField);
            nodesField.AddToClassList("unitydots-separated-list");

            typeField.RegisterCallback<ChangeEvent<Enum>>(e =>
            {
                if (e.newValue is SplineType type)
                {
                    UpdateInspectorGUI(type);
                }
            });

            samplingField.RegisterCallback<ChangeEvent<Enum>>(e =>
            {
                if (e.newValue is SamplingMethod type)
                {
                    UpdateInspectorGUI(type);
                }
            });

            UpdateInspectorGUI(Target.Type);
            UpdateInspectorGUI(Target.SamplingMethod);
            UpdateDrawer(true);

            return container;
        }

        private void UpdateInspectorGUI(SplineType value)
        {
            _alphaSlider.style.display = (value == SplineType.CatmullRom)
                    ? DisplayStyle.Flex
                    : DisplayStyle.None;
        }

        private void UpdateInspectorGUI(SamplingMethod value)
        {
            _samplingMaxErrorSlider.style.display = (value == SamplingMethod.Adaptive)
                    ? DisplayStyle.Flex
                    : DisplayStyle.None;
            _samplingFrequencySlider.style.display = (value == SamplingMethod.Uniform)
                    ? DisplayStyle.Flex
                    : DisplayStyle.None;
        }

        private void OnDisable()
        {
            _prevToolType = Tools.current.HasFlag(Tool.Custom) ? ToolManager.activeToolType : null;
            DisposeDrawer();
        }

        private void OnSceneGUI()
        {
            if (_prevToolType != null)
            {
                ToolManager.SetActiveTool(_prevToolType);
                _prevToolType = null;
            }

            UpdateDrawer(false);

            if (_drawer != null)
            {
                _drawer.DrawSpline();
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }
        }

        private void DisposeDrawer()
        {
            if (_drawer is IDisposable)
            {
                (_drawer as IDisposable).Dispose();
            }
            _drawer = null;
        }

        private void UpdateDrawer(bool force)
        {
            if (force || _previousType != Target.Type)
            {
                DisposeDrawer();
                switch (Target.Type)
                {
                    case SplineType.Path:
                        _drawer = new PathDrawer { Target = Target };
                        break;
                    case SplineType.Bezier:
                        _drawer = new BezierDrawer { Target = Target };
                        break;
                    case SplineType.CatmullRom:
                        _drawer = new CatmullRomDrawer { Target = Target };
                        break;
                }
                _previousType = Target.Type;
            }

            if (_drawer != null)
            {
                _drawer.Update();

                if (_infoText != null)
                {
                    _infoText.text = _drawer.SampleCount.ToString();
                }
            }
        }
    }
}
