using UnityDots.Editor;
using UnityDots.Splines.Authoring;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UnityDots.Splines.Editors
{
    [CustomPropertyDrawer(typeof(Node))]
    public sealed class NodeDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new Foldout
            {
                text = "Node"
            };

            var positionField = new PropertyField(property.FindPropertyRelative("Position"));
            var scaleField = new PropertyField(property.FindPropertyRelative("Scale"));
            var rollSlider = new SliderField("Roll", 0, 360, 0)
            {
                BindingPath = "Roll"
            };
            var startTangentField = new PropertyField(property.FindPropertyRelative("StartTangent"));
            var endTangentField = new PropertyField(property.FindPropertyRelative("EndTangent"));

            container.Add(positionField);
            container.Add(scaleField);
            container.Add(rollSlider);
            container.Add(startTangentField);
            container.Add(endTangentField);
            container.BindProperty(property);

            return container;
        }
    }
}
