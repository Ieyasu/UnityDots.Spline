using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Splines
{
    /// <summary>Array of generated discrete samples on the spline.</summary>
    [InternalBufferCapacity(0)]
    public struct SplineSample : IBufferElementData
    {
        /// Position of the sample.
        public float3 Position;

        /// Tangent of the sample.
        public float3 Tangent;

        /// Scale of the sample.
        public float2 Scale;

        /// Roll or twist of the sample.
        public float Roll;

        /// Rotation of the sample without roll applied.
        public quaternion Rotation { get; internal set; }

        /// The interpolation value of the sample.
        public float Time { get; internal set; }

        /// The rotation of the sample with roll applied.
        public quaternion RotationWithRoll => math.mul(Rotation, quaternion.AxisAngle(new float3(0, 0, 1), Roll));

        /// The normal of the sample.
        public float3 Normal => math.mul(Rotation, new float3(0, 1, 0));

        /// The binormal of the sample.
        public float3 Binormal => math.mul(Rotation, new float3(1, 0, 0));

        /// <summary>Returns a new linearly interpolated sample from a list of existing samples.</summary>
        /// <param name="t">Value between [0,1] where 0 is the first sample and 1 the last.</param>
        /// <param name="samples">Array of samples to use for interpolation.</param>
        public static SplineSample Interpolate(float t, NativeArray<SplineSample> samples)
        {
            var startIndex = (int)math.clamp(t * (samples.Length - 1), 0, samples.Length - 2);
            var startSample = samples[startIndex];

            var delta = (startSample.Time < t) ? 1 : -1;
            for (var i = startIndex; i < samples.Length - 1 && i >= 0; i += delta)
            {
                startSample = samples[i];
                var endSample = samples[i + 1];
                if (endSample.Time >= t && t >= startSample.Time)
                {
                    t = (t - startSample.Time) / (endSample.Time - startSample.Time);
                    return new SplineSample
                    {
                        Position = math.lerp(startSample.Position, endSample.Position, t),
                        Tangent = math.normalize(math.lerp(startSample.Tangent, endSample.Tangent, t)),
                        Scale = math.lerp(startSample.Scale, endSample.Scale, t),
                        Roll = math.lerp(startSample.Roll, endSample.Roll, t),
                        Rotation = math.slerp(startSample.Rotation, endSample.Rotation, t),
                        Time = math.lerp(startSample.Time, endSample.Time, t)
                    };
                }
            }

            return samples[0];
        }
    }
}
