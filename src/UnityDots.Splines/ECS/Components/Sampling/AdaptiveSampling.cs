using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>Component for controlling adaptive sampling of the spline.</summary>
    public struct AdaptiveSampling : IComponentData, ISamplingMethod
    {
        /// Maximum error when using adaptive sampling.
        public float MaxError;
    }
}
