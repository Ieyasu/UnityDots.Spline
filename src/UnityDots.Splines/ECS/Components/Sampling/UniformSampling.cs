using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>Component for controlling uniform sampling of the spline.</summary>
    public struct UniformSampling : IComponentData, ISamplingMethod
    {
        /// Distance between adjacent samples when using uniform sampling.
        public float Frequency;
    }
}
