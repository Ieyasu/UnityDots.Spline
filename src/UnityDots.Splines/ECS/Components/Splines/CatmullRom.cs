using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>Catmull rom spline.</summary>
    public struct CatmullRom : IComponentData, ISpline
    {
        /// Alpha value of the catmull rom spline.
        public float Alpha;
    }
}
