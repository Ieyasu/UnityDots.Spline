using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>Path spline. Path consists of straight lines between control points.</summary>
    public struct Path : IComponentData, ISpline
    {
    }
}
