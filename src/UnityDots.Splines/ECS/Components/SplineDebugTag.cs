using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>Component for displaying spline control point normals in the scene view.</summary>
    public struct SplineDebugNormalsTag : IComponentData
    {
    }

    /// <summary>Component for displaying spline control point tangents in the scene view.</summary>
    public struct SplineDebugTangentsTag : IComponentData
    {
    }

    /// <summary>Component for displaying spline control point positions in the scene view.</summary>
    public struct SplineDebugPositionsTag : IComponentData
    {
    }
}
