using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Splines
{
    /// <summary>The control points of a spline.</summary>
    [InternalBufferCapacity(0)]
    public struct ControlPoint : IBufferElementData
    {
        /// The position of the control point.
        public float3 Position;

        /// The scale of the control point.
        public float2 Scale;

        /// The roll or twist of the control point.
        public float Roll;
    }
}
