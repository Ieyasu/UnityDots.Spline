using Unity.Entities;

namespace UnityDots.Splines
{
    /// <summary>System group for all spline sampler systems.</summary>
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public sealed class SplineSamplerSystemGroup : ComponentSystemGroup
    {
    }
}
