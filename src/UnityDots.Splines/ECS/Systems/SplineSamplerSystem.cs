using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityDots.Splines;

[assembly: RegisterGenericJobType(typeof(SplineSamplerJob<Path, PathSampleProvider>))]
[assembly: RegisterGenericJobType(typeof(SplineSamplerJob<Bezier, BezierSampleProvider>))]
[assembly: RegisterGenericJobType(typeof(SplineSamplerJob<CatmullRom, CatmullRomSampleProvider>))]

namespace UnityDots.Splines
{
    /// <summary>System for creating a discrete sampling of paths.</summary>
    [ExecuteAlways]
    public sealed class PathSamplerSystem : SplineSamplerSystem<Path, PathSampleProvider>
    {

    }

    /// <summary>System for creating a discrete sampling of bezier splines.</summary>
    [ExecuteAlways]
    public sealed class BezierSamplerSystem : SplineSamplerSystem<Bezier, BezierSampleProvider>
    {

    }

    /// <summary>System for creating a discrete sampling of catmull rom splines.</summary>
    [ExecuteAlways]
    public sealed class CatmullRomSamplerSystem : SplineSamplerSystem<CatmullRom, CatmullRomSampleProvider>
    {

    }

    [BurstCompile]
    internal struct SplineSamplerJob<TSpline, TSampleProvider> : IJobChunk
                where TSpline : struct, ISpline, IComponentData
                where TSampleProvider : struct, ISampleProvider<TSpline>
    {
        public bool IsZeroSized;

        [WriteOnly] public BufferTypeHandle<SplineSample> SampleType;

        [ReadOnly] public ComponentTypeHandle<TSpline> SplineType;
        [ReadOnly] public ComponentTypeHandle<AdaptiveSampling> AdaptiveSamplingType;
        [ReadOnly] public ComponentTypeHandle<UniformSampling> UniformSamplingType;
        [ReadOnly] public BufferTypeHandle<ControlPoint> ControlPointType;

        public SplineSamplerJob(JobComponentSystem system)
        {
            IsZeroSized = ComponentType.ReadOnly<TSpline>().IsZeroSized;
            SampleType = system.GetBufferTypeHandle<SplineSample>();
            SplineType = system.GetComponentTypeHandle<TSpline>(true);
            AdaptiveSamplingType = system.GetComponentTypeHandle<AdaptiveSampling>(true);
            UniformSamplingType = system.GetComponentTypeHandle<UniformSampling>(true);
            ControlPointType = system.GetBufferTypeHandle<ControlPoint>(true);
        }

        public static EntityQueryDesc GetEntityQueryDesc()
        {
            return new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    typeof(SplineSample),
                    ComponentType.ReadOnly<TSpline>(),
                    ComponentType.ReadOnly<ControlPoint>()
                },
                Any = new ComponentType[]
                {
                    ComponentType.ReadOnly<AdaptiveSampling>(),
                    ComponentType.ReadOnly<UniformSampling>()
                }
            };
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var samplesAccessor = chunk.GetBufferAccessor(SampleType);
            var controlPointsAccessor = chunk.GetBufferAccessor(ControlPointType);
            var useAdaptiveSampling = chunk.Has(AdaptiveSamplingType);
            var useUniformSampling = chunk.Has(UniformSamplingType);

            // Both modes not supported.
            if (useAdaptiveSampling && useUniformSampling)
            {
                return;
            }

            var adaptiveSamplingMethods = useAdaptiveSampling ? chunk.GetNativeArray(AdaptiveSamplingType) : default;
            var uniformSamplingMethods = useUniformSampling ? chunk.GetNativeArray(UniformSamplingType) : default;
            var splines = !IsZeroSized ? chunk.GetNativeArray(SplineType) : default;

            var sampler = new SplineSampler<TSpline, TSampleProvider>(Allocator.Temp);
            var samples = new NativeList<SplineSample>(Allocator.Temp);

            for (var i = 0; i < chunk.Count; ++i)
            {
                var spline = IsZeroSized ? default : splines[i];
                var controlPoints = controlPointsAccessor[i].AsNativeArray();
                sampler.SetSpline(spline, controlPoints);

                if (useAdaptiveSampling)
                {
                    var maxError = adaptiveSamplingMethods[i].MaxError;
                    sampler.SampleAdaptive(maxError, samples);
                }
                else if (useUniformSampling)
                {
                    var frequency = uniformSamplingMethods[i].Frequency;
                    sampler.SampleUniform(frequency, samples);
                }

                samplesAccessor[i].CopyFrom(samples.AsArray());
            }

            sampler.Dispose();
            samples.Dispose();
        }
    }

    /// <summary>System for creating a discrete sampling of splines.</summary>
    /// <param name="TSpline">The spline type to sample.</param>
    /// <param name="TSampleProvider">The sample provider to use for sampling.</param>
    [ExecuteAlways]
    [UpdateInGroup(typeof(SplineSamplerSystemGroup))]
    public abstract class SplineSamplerSystem<TSpline, TSampleProvider> : JobComponentSystem
                where TSpline : struct, ISpline, IComponentData
                where TSampleProvider : struct, ISampleProvider<TSpline>
    {
        private EntityQuery _query;

        protected override void OnCreate()
        {
            _query = GetEntityQuery(SplineSamplerJob<TSpline, TSampleProvider>.GetEntityQueryDesc());
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            return new SplineSamplerJob<TSpline, TSampleProvider>(this).Schedule(_query, inputDependencies);
        }
    }
}
