using Unity.Entities;
using UnityEngine;

namespace UnityDots.Splines
{
    /// <summary>System for drawing spline debug information in the scene view.</summary>
    [ExecuteAlways]
    // Update before so that we have the whole frame time to calculate the samples.
    [UpdateBefore(typeof(SplineSamplerSystemGroup))]
    public sealed class SplineDebugSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((DynamicBuffer<SplineSample> samples, ref SplineDebugPositionsTag _) =>
            {
                for (var i = 0; i < samples.Length - 2; ++i)
                {
                    var sample = samples[i];
                    Debug.DrawLine(sample.Position, samples[i + 1].Position, Color.green);
                }
            });

            Entities.ForEach((DynamicBuffer<SplineSample> samples, ref SplineDebugNormalsTag _) =>
            {
                for (var i = 0; i < samples.Length - 1; ++i)
                {
                    var sample = samples[i];
                    Debug.DrawRay(sample.Position, 0.2f * sample.Normal, Color.magenta);
                }
            });

            Entities.ForEach((DynamicBuffer<SplineSample> samples, ref SplineDebugTangentsTag _) =>
            {
                for (var i = 0; i < samples.Length - 1; ++i)
                {
                    var sample = samples[i];
                    Debug.DrawRay(sample.Position, 0.2f * sample.Tangent, Color.blue);
                }
            });
        }
    }
}
