using Unity.Collections;
using Unity.Mathematics;
using UnityDots.Mathematics;

namespace UnityDots.Splines
{
    /// <summary><c>ISampleProvider</c> implementation for the <c>Bezier</c> spline type.</summary>
    public struct BezierSampleProvider : ISampleProvider<Bezier>
    {
        /// The spline component to sample.
        public Bezier Spline { get; set; }

        /// The control points of the spline to sample.
        public NativeArray<ControlPoint> ControlPoints { get; set; }

        /// The number of segments on the spline to sample.
        public int SegmentCount => (ControlPoints.Length - 1) / 3;

        /// <summary>Returns the length of a spline segment.</summary>
        /// <param name="segmentIndex">The index of the segment.</param>
        public float GetSegmentLength(int segmentIndex)
        {
            var index = 3 * segmentIndex;
            var p0 = ControlPoints[index].Position;
            var p1 = ControlPoints[index + 1].Position;
            var p2 = ControlPoints[index + 2].Position;
            var p3 = ControlPoints[index + 3].Position;
            return Geometry.GetLengthBezierCubic(p0, p1, p2, p3);
        }

        /// <summary>Samples a spline segment.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the segment and 1 the end.</param>
        /// <param name="segmentIndex">The index of the segment.</param>
        public SplineSample Sample(float t, int segmentIndex)
        {
            var index = 3 * segmentIndex;
            var p0 = ControlPoints[index];
            var p1 = ControlPoints[index + 1];
            var p2 = ControlPoints[index + 2];
            var p3 = ControlPoints[index + 3];

            // Coefficients.
            var tt = t * t;
            var ttt = tt * t;
            var tinv = 1 - t;
            var ttinv = tinv * tinv;
            var tttinv = ttinv * tinv;
            var ttinv_t_3 = ttinv * 3 * t;
            var tinv_tt_3 = tinv * tt * 3;

            var position = (p0.Position * tttinv) +
                    (p1.Position * ttinv_t_3) +
                    (p2.Position * tinv_tt_3) +
                    (p3.Position * ttt);

            var tangent = ((p1.Position - p0.Position) * (ttinv * 3)) +
                   ((p2.Position - p1.Position) * (tinv * t * 6)) +
                   ((p3.Position - p2.Position) * (tt * 3));

            // Calculate scale and roll in one for better vectorization.
            var sr0 = new float3(p0.Scale.x, p0.Scale.y, p0.Roll);
            var sr1 = new float3(p3.Scale.x, p3.Scale.y, p3.Roll);
            var sr = math.lerp(sr0, sr1, t);

            return new SplineSample
            {
                Position = position,
                Tangent = math.normalize(tangent),
                Scale = sr.xy,
                Roll = sr.z
            };
        }
    }
}
