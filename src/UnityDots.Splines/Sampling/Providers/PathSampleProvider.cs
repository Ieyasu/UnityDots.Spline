using Unity.Collections;
using Unity.Mathematics;

namespace UnityDots.Splines
{
    /// <summary><c>ISampleProvider</c> implementation for the <c>Path</c> spline type.</summary>
    public struct PathSampleProvider : ISampleProvider<Path>
    {
        /// The spline component to sample.
        public Path Spline { get; set; }

        /// The control points of the spline to sample.
        public NativeArray<ControlPoint> ControlPoints { get; set; }

        /// The number of segments on the spline to sample.
        public int SegmentCount => ControlPoints.Length - 1;

        /// <summary>Returns the length of a spline segment.</summary>
        /// <param name="segmentIndex">The index of the segment.</param>
        public float GetSegmentLength(int segmentIndex)
        {
            var p0 = ControlPoints[segmentIndex].Position;
            var p1 = ControlPoints[segmentIndex + 1].Position;
            return math.length(p0 - p1);
        }

        /// <summary>Samples a spline segment.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the segment and 1 the end.</param>
        /// <param name="segmentIndex">The index of the segment.</param>
        public SplineSample Sample(float t, int segmentIndex)
        {
            var p0 = ControlPoints[segmentIndex];
            var p1 = ControlPoints[segmentIndex + 1];

            var position = math.lerp(p0.Position, p1.Position, t);
            var tangent = math.normalize(p1.Position - p0.Position);
            var scale = math.lerp(p0.Scale, p1.Scale, t);
            var roll = math.lerp(p0.Roll, p1.Roll, t);

            return new SplineSample
            {
                Position = position,
                Tangent = tangent,
                Scale = scale.xy,
                Roll = roll
            };
        }
    }
}
