using Unity.Collections;
using Unity.Mathematics;
using UnityDots.Mathematics;

namespace UnityDots.Splines
{
    /// <summary><c>ISampleProvider</c> implementation for the <c>CatmullRom</c> spline type.</summary>
    public struct CatmullRomSampleProvider : ISampleProvider<CatmullRom>
    {
        /// The spline component to sample.
        public CatmullRom Spline { get; set; }

        /// The control points of the spline to sample.
        public NativeArray<ControlPoint> ControlPoints { get; set; }

        /// The number of segments on the spline to sample.
        public int SegmentCount => ControlPoints.Length - 3;

        /// <summary>Returns the length of a spline segment.</summary>
        /// <param name="segmentIndex">The index of the segment.</param>
        public float GetSegmentLength(int segmentIndex)
        {
            var p0 = ControlPoints[segmentIndex].Position;
            var p1 = ControlPoints[segmentIndex + 1].Position;
            var p2 = ControlPoints[segmentIndex + 2].Position;
            var p3 = ControlPoints[segmentIndex + 3].Position;
            return Geometry.GetLengthCatmullRom(p0, p1, p2, p3);
        }

        /// <summary>Samples a spline segment.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the segment and 1 the end.</param>
        /// <param name="segmentIndex">The index of the segment.</param>
        public SplineSample Sample(float t, int segmentIndex)
        {
            var p0 = ControlPoints[segmentIndex];
            var p1 = ControlPoints[segmentIndex + 1];
            var p2 = ControlPoints[segmentIndex + 2];
            var p3 = ControlPoints[segmentIndex + 3];

            float t0, t1, t2, t3;
            if (Spline.Alpha == 0.5f)
            {
                t0 = 0.0f;
                t1 = math.sqrt(math.length(p0.Position - p1.Position)) + t0;
                t2 = math.sqrt(math.length(p1.Position - p2.Position)) + t1;
                t3 = math.sqrt(math.length(p2.Position - p3.Position)) + t2;
            }
            else if (Spline.Alpha == 1.0f)
            {
                t0 = 0.0f;
                t1 = math.length(p0.Position - p1.Position) + t0;
                t2 = math.length(p1.Position - p2.Position) + t1;
                t3 = math.length(p2.Position - p3.Position) + t2;
            }
            else if (Spline.Alpha == 0.0f)
            {
                t0 = 0.0f;
                t1 = 1 + t0;
                t2 = 1 + t1;
                t3 = 1 + t2;
            }
            else
            {
                t0 = 0.0f;
                t1 = math.pow(math.length(p1.Position - p0.Position), Spline.Alpha) + t0;
                t2 = math.pow(math.length(p2.Position - p1.Position), Spline.Alpha) + t1;
                t3 = math.pow(math.length(p3.Position - p2.Position), Spline.Alpha) + t2;
            }

            var td = ((1 - t) * t1) + (t * t2);
            var tt0 = td - t0;
            var tt1 = td - t1;
            var tt2 = td - t2;
            var t1t = t1 - td;
            var t2t = t2 - td;
            var t3t = t3 - td;
            var t1t0 = 1.0f / (t1 - t0);
            var t2t0 = 1.0f / (t2 - t0);
            var t2t1 = 1.0f / (t2 - t1);
            var t3t1 = 1.0f / (t3 - t1);
            var t3t2 = 1.0f / (t3 - t2);

            var pa0 = (t1t * t1t0 * p0.Position) + (tt0 * t1t0 * p1.Position);
            var pa1 = (t2t * t2t1 * p1.Position) + (tt1 * t2t1 * p2.Position);
            var pa2 = (t3t * t3t2 * p2.Position) + (tt2 * t3t2 * p3.Position);
            var pb0 = (t2t * t2t0 * pa0) + (tt0 * t2t0 * pa1);
            var pb1 = (t3t * t3t1 * pa1) + (tt1 * t3t1 * pa2);
            var position = (t2t * t2t1 * pb0) + (tt1 * t2t1 * pb1);

            var da0 = t1t0 * (p1.Position - p0.Position);
            var da1 = t2t1 * (p2.Position - p1.Position);
            var da2 = t3t2 * (p3.Position - p2.Position);
            var db0 = (t2t0 * (pa1 - pa0)) + (t2t * t2t0 * da0) + (tt0 * t2t0 * da1);
            var db1 = (t3t1 * (pa2 - pa1)) + (t3t * t3t1 * da1) + (tt1 * t3t1 * da2);
            var tangent = (t2t1 * (pb1 - pb0)) + (t2t * t2t1 * db0) + (tt1 * t2t1 * db1);

            // Calculate scale and roll in one for better vectorization.
            var sr0 = new float3(p0.Scale.x, p0.Scale.y, p0.Roll);
            var sr1 = new float3(p1.Scale.x, p1.Scale.y, p1.Roll);
            var sr2 = new float3(p2.Scale.x, p2.Scale.y, p2.Roll);
            var sr3 = new float3(p3.Scale.x, p3.Scale.y, p3.Roll);
            var sa0 = (t1t * t1t0 * sr0) + (tt0 * t1t0 * sr1);
            var sa1 = (t2t * t2t1 * sr1) + (tt1 * t2t1 * sr2);
            var sa2 = (t3t * t3t2 * sr2) + (tt2 * t3t2 * sr3);
            var sb0 = (t2t * t2t0 * sa0) + (tt0 * t2t0 * sa1);
            var sb1 = (t3t * t3t1 * sa1) + (tt1 * t3t1 * sa2);
            var sr = (t2t * t2t1 * sb0) + (tt1 * t2t1 * sb1);

            return new SplineSample
            {
                Position = position,
                Tangent = math.normalize(tangent),
                Scale = sr.xy,
                Roll = sr.z
            };
        }
    }
}
