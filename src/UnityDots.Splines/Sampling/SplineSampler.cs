using System;
using Unity.Collections;
using Unity.Mathematics;

namespace UnityDots.Splines
{
    /// <summary>Handles sampling of splines.</summary>
    /// <param name="TSpline">The spline type to sample.</param>
    /// <param name="TSampleProvider">The sample provider to use for sampling.</param>
    public struct SplineSampler<TSpline, TSampleProvider> : IDisposable
            where TSpline : struct, ISpline
            where TSampleProvider : struct, ISampleProvider<TSpline>
    {
        private float _splineLength;
        private TSampleProvider _sampleProvider;
        private NativeList<float> _segmentLengths;

        /// Returns whether memory for the sampler is allocated.
        public bool IsCreated => _segmentLengths.IsCreated;

        /// <summary>Creates a new SplineSampler object.</summary>
        /// <param name="allocator">Allocator used for allocating memory.</param>
        public SplineSampler(Allocator allocator)
        {
            _splineLength = 0;
            _sampleProvider = default;
            _segmentLengths = new NativeList<float>(allocator);
        }

        /// <summary>Sets the spline that should be sampled next.</summary>
        /// <param name="spline">The spline to sample.</param>
        /// <param name="controlPoints">The control points of the spline to sample.</param>
        public void SetSpline(TSpline spline, NativeArray<ControlPoint> controlPoints)
        {
            _sampleProvider = default;
            _sampleProvider.Spline = spline;
            _sampleProvider.ControlPoints = controlPoints;
            CalculateSegmentLengths();
        }

        /// <summary>Sets the spline provider to use for sampling.</summary>
        /// <param name="sampleProvider">The ISamplerProvider to use for sampling.</param>
        public void SetSampleProvider(TSampleProvider sampleProvider)
        {
            _sampleProvider = sampleProvider;
            CalculateSegmentLengths();
        }

        /// <summary>Deallocates any allocated memory.</summary>
        public void Dispose()
        {
            if (_segmentLengths.IsCreated)
            {
                _segmentLengths.Dispose();
            }
        }

        /// <summary>Returns a uniformly taken sample on the spline.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the spline and 1 the end.</param>
        public SplineSample SampleUniform(float t)
        {
            var sample = SampleUniformNoRotation(t);
            sample.Rotation = CalculateRotation(sample);
            sample.Time = t;
            return sample;
        }

        /// <summary>Returns a uniformly taken sample on the spline. Interpolates the spline using a previous sample
        /// as a reference for normal calculation. This ensures continuous normals without sudden flips.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the spline and 1 the end.</param>
        public SplineSample SampleUniform(float t, SplineSample prevSample)
        {
            var sample = SampleUniformNoRotation(t);
            sample.Rotation = CalculateRotation(sample, prevSample);
            sample.Time = t;
            return sample;
        }

        /// <summary>Returns multiple uniformly taken samples on the spline.</summary>
        /// <param name="samples">Array of samples to fill.</param>
        public void SampleUniform(NativeArray<SplineSample> samples)
        {
            SampleUniform(samples.Length, samples);
        }

        /// <summary>Returns multiple uniformly taken samples on the spline.</summary>
        /// <param name="sampleCount">Number of sample to take.</param>
        /// <param name="samples">Array of samples to fill.</param>
        public void SampleUniform(int sampleCount, NativeArray<SplineSample> samples)
        {
            if (sampleCount == 0)
            {
                return;
            }

            samples[0] = SampleUniform(0);
            for (var i = 1; i < sampleCount; ++i)
            {
                var t = i / (float)(sampleCount - 1);
                samples[i] = SampleUniform(t, samples[i - 1]);
            }
        }

        /// <summary>Returns multiple uniformly taken samples on the spline.</summary>
        /// <param name="sampleFrequency">Distance between adjacent samples.</param>
        /// <param name="samples">List of samples to fill.</param>
        public void SampleUniform(float sampleFrequency, NativeList<SplineSample> samples)
        {
            var sampleCount = (int)math.ceil(_splineLength / sampleFrequency);
            samples.ResizeUninitialized(sampleCount);
            SampleUniform(sampleCount, samples.AsArray());
        }

        /// <summary>Returns multiple adaptively taken samples on the spline.</summary>
        /// <param name="maxError">Maximum error  [0,1]. Smaller error increases the number of samples.</param>
        /// <param name="samples">List of samples to fill.</param>
        public void SampleAdaptive(float maxError, NativeList<SplineSample> samples)
        {
            // Max error is a value between 0 and 1. This gives better controllability.
            maxError = maxError * maxError * maxError;

            samples.Clear();
            for (var i = 0; i < _segmentLengths.Length; ++i)
            {
                var start = _sampleProvider.Sample(0, i);
                var end = _sampleProvider.Sample(1, i);
                start.Time = i > 0 ? _segmentLengths[i - 1] : 0;
                end.Time = _segmentLengths[i];

                var timeScale = 1.0f / (end.Time - start.Time);
                SampleAdaptive(start, end, i, timeScale, start.Time, maxError, samples);

                if (i == _segmentLengths.Length - 1)
                {
                    end.Rotation = CalculateRotation(end, samples[samples.Length - 1]);
                    samples.Add(end);
                }
            }
        }

        private void SampleAdaptive(SplineSample start, SplineSample end, int segmentIndex, float timeScale, float startTime, float maxError, NativeList<SplineSample> samples)
        {
            var epsilon = 0.01f / _splineLength * timeScale;

            var t = 0.5f * (end.Time + start.Time);
            var middle = _sampleProvider.Sample((t - startTime) * timeScale, segmentIndex);
            middle.Time = t;

            // If error is small enough, we can stop.
            // We use the area of the parallelogram that forms between the start, end and middle points as error heuristic.
            var startDelta = start.Position - middle.Position;
            var endDelta = end.Position - middle.Position;
            var startDeltaLength = math.length(startDelta);
            var endDeltaLength = math.length(endDelta);
            var error1 = math.lengthsq(math.cross(start.Tangent * math.sqrt(startDeltaLength), middle.Tangent));
            var error2 = math.lengthsq(math.cross(end.Tangent * math.sqrt(endDeltaLength), middle.Tangent));
            var error = error1 + error2;
            if (error <= maxError || end.Time - start.Time < epsilon)
            {
                start.Rotation = samples.Length == 0 ? CalculateRotation(start) : CalculateRotation(start, samples[samples.Length - 1]);
                samples.Add(start);
                return;
            }

            SampleAdaptive(start, middle, segmentIndex, timeScale, startTime, maxError, samples);
            SampleAdaptive(middle, end, segmentIndex, timeScale, startTime, maxError, samples);
        }

        private void CalculateSegmentLengths()
        {
            _segmentLengths.ResizeUninitialized(_sampleProvider.SegmentCount);

            _splineLength = 0.0f;
            for (var i = 0; i < _segmentLengths.Length; ++i)
            {
                _splineLength += _sampleProvider.GetSegmentLength(i);
                _segmentLengths[i] = _splineLength;
            }

            // Normalize to [0, 1].
            for (var i = 0; i < _segmentLengths.Length - 1; ++i)
            {
                _segmentLengths[i] /= _splineLength;
            }

            // Make sure the last length is always 1.
            if (_segmentLengths.Length > 0)
            {
                _segmentLengths[_segmentLengths.Length - 1] = 1;
            }
        }

        private static quaternion CalculateRotation(SplineSample sample)
        {
            var binormal = new float3(-sample.Tangent.z, 0, sample.Tangent.x);
            if (binormal.Equals(float3.zero))
            {
                binormal = new float3(0, 0, 1);
            }
            var normal = math.normalizesafe(math.cross(binormal, sample.Tangent));
            return quaternion.LookRotation(sample.Tangent, normal);
        }

        private static quaternion CalculateRotation(SplineSample sample, SplineSample prevSample)
        {
            var v1 = sample.Position - prevSample.Position;
            var c1 = math.dot(v1, v1);
            if (c1 == 0)
            {
                return CalculateRotation(sample);
            }

            // Reflect previous samples tangent and binormal onto new sample.
            var prevBinormal = prevSample.Binormal;
            var binormalRefl = prevBinormal - (2.0f / c1 * math.dot(v1, prevBinormal) * v1);
            var tangentRefl = prevSample.Tangent - (2.0f / c1 * math.dot(v1, prevSample.Tangent) * v1);

            // Reflect a second time, over a plane at new sample, so that
            // the frame tangent is aligned with the curve tangent again.
            var v2 = sample.Tangent - tangentRefl;
            var c2 = math.dot(v2, v2);
            if (c2 == 0)
            {
                return CalculateRotation(sample);
            }

            var binormal = (2.0f / c2 * math.dot(v2, binormalRefl) * v2) - binormalRefl;
            var normal = math.normalizesafe(math.cross(binormal, sample.Tangent));
            return quaternion.LookRotation(sample.Tangent, normal);
        }

        private SplineSample SampleUniformNoRotation(float t)
        {
            t = math.clamp(t, 0, 1);
            var segmentIndex = FindSegmentIndex(t);
            if (segmentIndex == -1)
            {
                return new SplineSample();
            }

            t = ArcLengthReparametrize(t, segmentIndex);
            var sample = _sampleProvider.Sample(t, segmentIndex);
            return sample;
        }

        // Reparametrizes t so that it corresponds to the length of the spline.
        private float ArcLengthReparametrize(float t, int i)
        {
            var st = _segmentLengths[i];
            var pst = i > 0 ? _segmentLengths[i - 1] : 0.0f;
            return (st == pst) ? st : (t - pst) / (st - pst);
        }

        // Finds the index of a segement the given t lies on.
        private int FindSegmentIndex(float t)
        {
            var segmentCount = _segmentLengths.Length;
            if (segmentCount == 0)
            {
                return -1;
            }

            // Optimize by starting from an estimated index instead of looping the whole array.
            // This is probably a little bit slower for small splines, but makes sure the interpolation
            // is constant time even for large splines.
            var i = (int)math.clamp(t * segmentCount, 0, segmentCount - 1);
            var delta = (_segmentLengths[i] < t) ? 1 : -1;
            for (; i < _segmentLengths.Length && i >= 0; i += delta)
            {
                var prevT = (i > 0) ? _segmentLengths[i - 1] : 0;
                var nextT = _segmentLengths[i];
                if (nextT >= t && t >= prevT)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
