using Unity.Collections;

namespace UnityDots.Splines
{
    /// <summary>Common interface for spline sample providers.</summary>
    /// <param name="TSpline">The type of spline this sample provider can sample.</param>
    public interface ISampleProvider<TSpline> : ISplineSampleProvider
            where TSpline : struct, ISpline
    {
        /// The spline component to sample.
        TSpline Spline { get; set; }
    }

    /// <summary>Common interface for spline sample providers.</summary>
    public interface ISplineSampleProvider
    {
        /// The number of segments on the spline to sample.
        int SegmentCount { get; }

        /// The control points of the spline to sample.
        NativeArray<ControlPoint> ControlPoints { get; set; }

        /// <summary>Returns the length of a spline segment.</summary>
        /// <param name="segmentIndex">The index of the segment.</param>
        float GetSegmentLength(int segmentIndex);

        /// <summary>Samples a spline segment.</summary>
        /// <param name="t">Value between [0,1] where 0 is the start of the segment and 1 the end.</param>
        /// <param name="segmentIndex">The index of the segment.</param>
        SplineSample Sample(float t, int segmentIndex);
    }
}
