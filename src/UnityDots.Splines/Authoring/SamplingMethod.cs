namespace UnityDots.Splines.Authoring
{
    /// <summary>Type of method to use when sampling a spline.</summary>
    public enum SamplingMethod
    {
        Adaptive,
        Uniform
    }
}
