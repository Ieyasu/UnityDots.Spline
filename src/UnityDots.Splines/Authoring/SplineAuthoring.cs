using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Splines.Authoring
{
    /// <summary>Authoring MonoBehaviour for creating spline entities.</summary>
    [ExecuteAlways]
    public sealed class SplineAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public SplineType Type = SplineType.Bezier;
        public SamplingMethod SamplingMethod = SamplingMethod.Adaptive;
        public bool DebugNormals = false;
        public bool DebugTangents = false;
        public float DebugVectorLength = 0.3f;
        public float CatmullRomAlpha = 0.5f;
        public float SamplingFrequency = 0.2f;
        public float SamplingMaxError = 0.25f;
        public List<Node> Nodes;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddBuffer<SplineSample>(entity);

            if (DebugNormals)
            {
                dstManager.AddComponent<SplineDebugNormalsTag>(entity);
            }

            switch (SamplingMethod)
            {
                case SamplingMethod.Adaptive:
                    dstManager.AddComponentData(entity, new AdaptiveSampling { MaxError = SamplingMaxError });
                    break;
                case SamplingMethod.Uniform:
                    dstManager.AddComponentData(entity, new UniformSampling { Frequency = SamplingFrequency });
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(SamplingMethod.ToString());
            }

            var controlPoints = dstManager.AddBuffer<ControlPoint>(entity);
            switch (Type)
            {
                case SplineType.Path:
                    controlPoints.ResizeUninitialized(GetPathControlPointCount());
                    GetPathControlPoints(controlPoints.AsNativeArray());
                    dstManager.AddComponent<Path>(entity);
                    break;
                case SplineType.Bezier:
                    controlPoints.ResizeUninitialized(GetBezierControlPointCount());
                    GetBezierControlPoints(controlPoints.AsNativeArray());
                    dstManager.AddComponent<Bezier>(entity);
                    break;
                case SplineType.CatmullRom:
                    controlPoints.ResizeUninitialized(GetCatmullRomControlPointCount());
                    GetCatmullRomControlPoints(controlPoints.AsNativeArray());
                    dstManager.AddComponentData(entity, new CatmullRom { Alpha = CatmullRomAlpha });
                    break;
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(Type.ToString());
            }
        }

        public int GetPathControlPointCount()
        {
            return Nodes.Count;
        }

        public int GetBezierControlPointCount()
        {
            return 3 * (Nodes.Count) - 2;
        }

        public int GetCatmullRomControlPointCount()
        {
            return Nodes.Count + 2;
        }

        public void GetPathControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            if (GetPathControlPointCount() != controlPoints.Length)
            {
                Debug.LogError("Wrong amount of control points.");
                return;
            }

            for (var i = 0; i < Nodes.Count; ++i)
            {
                controlPoints[i] = GetControlPoint(Nodes[i]);
            }
        }

        public void GetBezierControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            if (GetBezierControlPointCount() != controlPoints.Length)
            {
                Debug.LogError("Wrong amount of control points.");
                return;
            }

            for (var i = 0; i < Nodes.Count; ++i)
            {
                controlPoints[3 * i] = GetControlPoint(Nodes[i]);

                // Add handles
                if (i > 0)
                {
                    controlPoints[3 * i - 1] = GetControlPoint(Nodes[i].StartHandle);
                }
                if (i < Nodes.Count - 1)
                {
                    controlPoints[3 * i + 1] = GetControlPoint(Nodes[i].EndHandle);
                }
            }
        }

        public void GetCatmullRomControlPoints(NativeArray<ControlPoint> controlPoints)
        {
            if (GetCatmullRomControlPointCount() != controlPoints.Length)
            {
                Debug.LogError("Wrong amount of control points.");
                return;
            }

            // Add an additional control point at the beginning and end.
            controlPoints[0] = new ControlPoint
            {
                Position = 2 * Nodes[0].Position - Nodes[1].Position,
                Scale = Nodes[0].Scale,
                Roll = math.radians(Nodes[0].Roll)
            };

            controlPoints[controlPoints.Length - 1] = new ControlPoint
            {
                Position = 2 * Nodes[Nodes.Count - 1].Position - Nodes[Nodes.Count - 2].Position,
                Scale = Nodes[Nodes.Count - 1].Scale,
                Roll = math.radians(Nodes[Nodes.Count - 1].Roll)
            };

            for (var i = 0; i < Nodes.Count; ++i)
            {
                controlPoints[i + 1] = GetControlPoint(Nodes[i]);
            }
        }

        private ControlPoint GetControlPoint(Node node)
        {
            return new ControlPoint
            {
                Position = node.Position,
                Scale = node.Scale,
                Roll = math.radians(node.Roll)
            };
        }

        private ControlPoint GetControlPoint(Vector3 handle)
        {
            return new ControlPoint
            {
                Position = handle,
                Scale = float2.zero,
                Roll = 0
            };
        }
    }
}
