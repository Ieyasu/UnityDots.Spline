namespace UnityDots.Splines.Authoring
{
    /// <summary>Type of spline to use.</summary>
    public enum SplineType
    {
        Path,
        Bezier,
        CatmullRom
    }
}
