using System;
using UnityEngine;

namespace UnityDots.Splines.Authoring
{
    /// <summary>Authoring data for spline control points.</summary>
    [Serializable]
    public sealed class Node
    {
        public Vector3 Position;
        public Vector2 Scale;
        public float Roll;

        public Vector3 StartTangent;
        public Vector3 EndTangent;

        public Vector3 StartHandle
        {
            get => StartTangent + Position;
            set => StartTangent = value - Position;
        }

        public Vector3 EndHandle
        {
            get => EndTangent + Position;
            set => EndTangent = value - Position;
        }

        public Node(Node node)
        {
            Position = node.Position;
            Scale = node.Scale;
            Roll = node.Roll;
            StartTangent = node.StartTangent;
            EndTangent = node.EndTangent;
        }
    }
}
